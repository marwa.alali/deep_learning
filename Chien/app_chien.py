import streamlit as stl
from fastai.vision.all import load_learner, Path, torch, PILImage
from PIL import Image

import pathlib
temp = pathlib.PosixPath
pathlib.PosixPath = pathlib.WindowsPath


model = load_learner('export.pkl')

stl.title("Quelle est la raçe de ce chien ?")
# Image =  PILImage.create(r'val/Border terrier/n02093754_3202.JPEG')
# stl.image(Image, caption="C'est !")

img_upload = stl.sidebar.file_uploader(
    "Sélectionner une image", type=['jpg', 'png','JPEG'])

if img_upload is not None:
    img_upload = PILImage.create(img_upload)
    stl.image(img_upload)
    if stl.button('Lancer analyse'):
        pred = model.predict(img_upload)[0]
        if pred == 'erreur':
            stl.warning("Est-ce un vraiment un chien")
        else:
            stl.success(f"L'analyse conclu qu'il s'agit d'un {pred}   ")
